import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Klingon Scripts Converter',
      theme: ThemeData(primarySwatch: Colors.red, fontFamily: 'Lakki'),
      darkTheme: ThemeData(
          primarySwatch: Colors.red,
          accentColor: Colors.red,
          brightness: Brightness.dark,
          fontFamily: 'Lakki'),
      themeMode: ThemeMode.dark,
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  TextEditingController tlhInganHolController = TextEditingController();
  TextEditingController xifanHolController = TextEditingController();
  TextEditingController unicodeController = TextEditingController();

  List textFields = [];

  String currentUnicodeText = '';

  AnimationController? bottomSheetController;

  double bottomSheetHeight = 256;

  @override
  void initState() {
    bottomSheetController = AnimationController(vsync: this);
    textFields = [
      TextField(
        controller: tlhInganHolController,
        onChanged: (newText) {
          handletlhInganHolChange();
        },
        minLines: 1,
        maxLines: 99999,
        decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'tlhIngan Hol input',
            suffixIcon: IconButton(
                icon: Icon(Icons.copy),
                onPressed: () {
                  copyText(tlhInganHolController.text);
                })),
      ),
      TextField(
        controller: xifanHolController,
        onChanged: (newText) {
          handlexifanHolChange();
        },
        minLines: 1,
        maxLines: 99999,
        decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'xifan Hol input',
            suffixIcon: IconButton(
                icon: Icon(Icons.copy),
                onPressed: () {
                  copyText(xifanHolController.text);
                })),
      ),
      TextField(
        controller: unicodeController,
        onChanged: (newText) {
          handleUnicodeChange();
        },
        minLines: 1,
        maxLines: 99999,
        style: TextStyle(fontFamily: 'HaSta'),
        decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'Unicode input',
            labelStyle: TextStyle(fontFamily: 'Lakki'),
            suffixIcon: IconButton(
                icon: Icon(Icons.copy),
                onPressed: () {
                  copyText(unicodeController.text);
                })),
      )
    ];

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Klingon Scripts Converter'),
      ),
      body: Center(
        child: Container(
          constraints: BoxConstraints(maxWidth: 768),
          child: Stack(children: [
            ListView.builder(
              itemBuilder: (c, i) => Card(
                  margin: EdgeInsets.all(8),
                  child: Padding(
                    child: textFields[i],
                    padding: EdgeInsets.all(16),
                  )),
              itemCount: textFields.length,
            ),
            DraggableScrollableSheet(
              minChildSize: 0.05,
              builder: (context, controller) {
                return BottomSheet(
                  elevation: 12,
                  onClosing: () {},
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(32),
                          topLeft: Radius.circular(32))),
                  builder: (context) => ListView(
                    padding: const EdgeInsets.fromLTRB(16, 8, 16, 16),
                    shrinkWrap: true,
                    controller: controller,
                    children: [
                      Center(
                        child: Container(
                          width: 64,
                          child: Divider(
                            thickness: 4,
                          ),
                        ),
                      ),
                      Text(
                        'Read in different fonts',
                        style: Theme.of(context).textTheme.headline5,
                      ),
                      Text(
                        'HaSta',
                        style: Theme.of(context).textTheme.headline6,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          currentUnicodeText,
                          style: TextStyle(fontFamily: 'HaSta'),
                        ),
                      ),
                      Text(
                        'vaHbo\'',
                        style: Theme.of(context).textTheme.headline6,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          currentUnicodeText,
                          style: TextStyle(fontFamily: 'vaHbo\''),
                        ),
                      ),
                      Text(
                        'Mandel',
                        style: Theme.of(context).textTheme.headline6,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          currentUnicodeText,
                          style: TextStyle(fontFamily: 'Mandel'),
                        ),
                      ),
                      Text(
                        'qolqoS',
                        style: Theme.of(context).textTheme.headline6,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          currentUnicodeText,
                          style: TextStyle(fontFamily: 'qolqoS'),
                        ),
                      ),
                    ],
                  ),
                );
              },
            )
          ]),
        ),
      ),

/*      BottomSheet(
        elevation: 12,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(32), topLeft: Radius.circular(32))),
        onDragStart: (details) {
          setState(() {
            bottomSheetHeight -= details.localPosition.dy;
          });
        },
        onDragEnd: (details, {isClosing}) {},
        onClosing: () {},
        animationController: bottomSheetController,
        enableDrag: true,
        builder: (c) => Container(
          height: bottomSheetHeight,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                width: 64,
                child: Divider(
                  thickness: 4,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: ListView(
                  shrinkWrap: true,
                  children: [
                    Text(
                      'Read in different fonts',
                      style: Theme.of(context).textTheme.headline5,
                    ),
                    Text(
                      'HaSta',
                      style: Theme.of(context).textTheme.headline6,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        currentUnicodeText,
                        style: TextStyle(fontFamily: 'HaSta'),
                      ),
                    ),
                    Text(
                      'vaHbo\'',
                      style: Theme.of(context).textTheme.headline6,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        currentUnicodeText,
                        style: TextStyle(fontFamily: 'vaHbo\''),
                      ),
                    ),
                    Text(
                      'Mandel',
                      style: Theme.of(context).textTheme.headline6,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        currentUnicodeText,
                        style: TextStyle(fontFamily: 'Mandel'),
                      ),
                    ),
                    Text(
                      'qolqoS',
                      style: Theme.of(context).textTheme.headline6,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        currentUnicodeText,
                        style: TextStyle(fontFamily: 'qolqoS'),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),*/
    );
  }

  void handletlhInganHolChange() {
    String xifanString = tlhInganHolController.text;
    tlhInganHoltoxifanhol.forEach((tlh, x) {
      xifanString = xifanString.replaceAll(tlh, x);
    });
    xifanHolController.text = xifanString;

    String unicodeString = tlhInganHolController.text;
    tlhInganHoltoUnicode.forEach((tlh, x) {
      unicodeString = unicodeString.replaceAll(tlh, x);
    });
    unicodeController.text = unicodeString;
    setState(() {
      currentUnicodeText = unicodeController.text;
    });
  }

  void handlexifanHolChange() {
    String tlhInganString = xifanHolController.text;
    tlhInganHoltoxifanhol.forEach((tlh, x) {
      tlhInganString = tlhInganString.replaceAll(x, tlh);
    });
    tlhInganHolController.text = tlhInganString;

    String unicodeString = tlhInganHolController.text;
    tlhInganHoltoUnicode.forEach((tlh, x) {
      unicodeString = unicodeString.replaceAll(tlh, x);
    });
    unicodeController.text = unicodeString;
    setState(() {
      currentUnicodeText = unicodeController.text;
    });
  }

  void handleUnicodeChange() {
    String tlhInganString = unicodeController.text;
    tlhInganHoltoUnicode.forEach((tlh, x) {
      tlhInganString = tlhInganString.replaceAll(x, tlh);
    });
    tlhInganHolController.text = tlhInganString;

    String xifanString = tlhInganHolController.text;
    tlhInganHoltoxifanhol.forEach((tlh, x) {
      xifanString = xifanString.replaceAll(tlh, x);
    });
    xifanHolController.text = xifanString;
    setState(() {
      currentUnicodeText = unicodeController.text;
    });
  }

  void copyText(String text) {
    Clipboard.setData(ClipboardData(text: text));
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text('Copied text to clipboard.'),
    ));
  }
}

const tlhInganHoltoxifanhol = {
  'tlh': 'x',
  'ch': 'c',
  'ng': 'f',
  'gh': 'g',
  'D': 'd',
  'H': 'h',
  'I': 'i',
  'q': 'k',
  'Q': 'q',
  'S': 's',
  '\'': 'z',
};

const tlhInganHoltoUnicode = {
  'tlh': '',
  'ch': '',
  'ng': '',
  'gh': '',
  'a': '',
  'b': '',
  'D': '',
  'e': '',
  'H': '',
  'I': '',
  'j': '',
  'q': '',
  'l': '',
  'm': '',
  'n': '',
  'o': '',
  'p': '',
  'Q': '',
  'r': '',
  'S': '',
  't': '',
  'u': '',
  'v': '',
  'w': '',
  'y': '',
  '\'': '',
  '.': '',
  ',': '',
  '0': '',
  '1': '',
  '2': '',
  '3': '',
  '4': '	',
  '5': '',
  '6': '',
  '7': '',
  '8': '',
  '9': '	',
};
